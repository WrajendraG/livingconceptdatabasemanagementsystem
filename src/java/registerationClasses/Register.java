package registerationClasses;

import requriedDatabaseConnection.DatabaseConnection;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Register extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException, ClassNotFoundException {

        String Day = null, Month = null, Year = null;
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            System.out.println("inside try");
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");

            out.println("<html>");
            System.out.println("inside html");
            out.println("<head>");
            out.println("<title>Servlet DemoClass</title>");
            out.println("</head>");
            out.println("<body>");

            System.out.println("outside html");

            Connection connection = DatabaseConnection.getConnection();
            //out.println("connection established");
            Statement st = connection.createStatement();
            // out.println("statement created");
            String query = "insert into client_info values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

            PreparedStatement ps = connection.prepareStatement(query);
            /////////////////////////
            String f = request.getParameter("fname");
            ps.setString(1, request.getParameter("fname"));
            ///////////////////////
            String l = request.getParameter("lname");
            ps.setString(2, request.getParameter("lname"));

            out.println("apartment" + request.getParameter("apartment"));
            ps.setString(3, request.getParameter("apartment"));

            out.println("area" + request.getParameter("area"));
            ps.setString(4, request.getParameter("area"));

            out.println("wings" + request.getParameter("wings"));
            ps.setString(5, request.getParameter("wings"));

            out.println("mobile1" + (request.getParameter("mobile1").toString()));
            ps.setString(6, (request.getParameter("mobile1")).toString());

            out.println("mobile2" + request.getParameter("mobile2"));
            ps.setString(7, (request.getParameter("mobile2")).toString());

            out.println("landline:" + request.getParameter("landline"));
            ps.setString(8, request.getParameter("landline"));

            out.println("city" + request.getParameter("city"));
            ps.setString(9, request.getParameter("city"));

            out.println("orderdate:" + request.getParameter("orderdate"));
            ps.setString(10, request.getParameter("orderdate"));
            
            String id = request.getParameter("installationdate");

            String[] d2 = id.split("-");  // length = 3     that why length - 1  used .if do not use array index out of bound errer generate. 
                 //  2017-12-28
            //index  0    1     2
            for (int i = d2.length - 1; i >= 0; i--) { 
     //1 time       i = 3-1 =2   ;   2>=0     ;  1
                Day = d2[2]; // 28  // this vriable not used in program.
     //2 time       i = 1   ;   1>=0     ;  0           
                Month = d2[1];   // 12
     //3 time       i = 0   ;   0>=0     ;  -1   next loop condication is false                      
                Year = d2[0];   // 2017

            }
            String sb = id.substring(0, 7);

            out.println("reversed date" + id.substring(0, 7));
            ps.setString(11, request.getParameter("installationdate"));

            out.println("entdoormodel" + request.getParameter("entdoormodel") + "\n");
            ps.setString(12, request.getParameter("entdoormodel"));

            out.println("totalentdoor:" + request.getParameter("totalentdoor") + "\n");
            ps.setString(13, request.getParameter("totalentdoor"));

            out.println("latch:" + request.getParameter("latch") + "\n");
            ps.setString(14, request.getParameter("latch"));

            out.println("tptcharges:" + Float.parseFloat(request.getParameter("tptcharges")) + "\n");
            ps.setFloat(15, Float.parseFloat(request.getParameter("tptcharges")));

            out.println("cost:" + Float.parseFloat(request.getParameter("cost")) + "\n");
            ps.setFloat(16, Float.parseFloat(request.getParameter("cost")));

            out.println("otherdoor:" + request.getParameter("otherdoor") + "\n");
            ps.setString(17, request.getParameter("otherdoor"));

            out.println("doornetqty:" + request.getParameter("doornetqty"));
            ps.setString(18, request.getParameter("doornetqty"));

            out.println("tpt2charges:" + Float.parseFloat(request.getParameter("tpt2charges")) + "\n");
            ps.setFloat(19, Float.parseFloat(request.getParameter("tpt2charges")));

            out.println("costfinalised:" + Float.parseFloat(request.getParameter("costfinalised")) + "\n");
            ps.setFloat(20, Float.parseFloat(request.getParameter("costfinalised")));

            out.println("otherproduct:" + request.getParameter("otherproduct") + "\n");
            ps.setString(21, request.getParameter("otherproduct"));

            out.println("opdate:" + request.getParameter("opdate"));
            ps.setString(22, request.getParameter("opdate"));

            out.println("cost:" + Float.parseFloat(request.getParameter("cost")) + "\n");
            ps.setFloat(23, Float.parseFloat(request.getParameter("cost")));

            out.println("netamount:" + Float.parseFloat(request.getParameter("netamount")) + "\n");
            ps.setFloat(24, Float.parseFloat(request.getParameter("netamount")));

            out.println("email:" + request.getParameter("email") + "\n");
            ps.setString(25, request.getParameter("email"));

            out.println("userid:" + request.getParameter("userid") + "\n");
            ps.setString(26, request.getParameter("userid"));
            //////////////////////////////
            String edi = request.getParameter("entrancedoor1");
            //out.println("entrancedoor:" + request.getParameter("entrancedoor"));
            ps.setString(27, request.getParameter("entrancedoor1"));

            out.println("totalterdoor1:" + request.getParameter("totalterdoor1"));
            // out.println("check:" + request.getParameter("totalterdoor"));
            ps.setString(28, request.getParameter("totalterdoor1"));

            out.println("year:" + request.getParameter("year"));
            ps.setString(29, request.getParameter("year"));

            String user_id = (f.substring(0, 3) + "_" + l.substring(0, 3) + "_" + edi + "Eind" + "_" + Month + "_" + Year);

            out.println("---------------------------" + user_id);
            String q2 = request.getParameter("radio1");
            out.println(request.getParameter("q2"));
            String value;
            if (q2.equals("individual")) {
                value = "individual";
            } else {
                value = "corporate";
            }
            out.println("\noptradio:=" + value);

            ps.setString(30, value);

            String q3 = request.getParameter("radio2");
            out.println(request.getParameter("q3"));
            String value1;
            if (q2.equals("new")) {
                value1 = "new";
            } else {
                value1 = "exiting";
            }
            out.println("category:=" + value1);

            ps.setString(31, value1);

            ps.executeUpdate();
            out.println("data saved");
            out.println("</body>");

            out.println("</html>");

            // Register r=new Register();
            // r.send2(user_id);
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/index.html");
            dispatcher.forward(request, response);

        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(Register.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Register.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(Register.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Register.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
