package registerationClasses;

import requriedDatabaseConnection.DatabaseConnection;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class userregister extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, SQLException {
        String fn = request.getParameter("firstname");
        String ln = request.getParameter("lastname");
        String bn = request.getParameter("businesstname");
        String mn = request.getParameter("mobileno");
        String eid = request.getParameter("emailid");
        String uid = request.getParameter("userid");
        String pwd = request.getParameter("password");

        Connection connection = DatabaseConnection.getConnection();

        Statement st = connection.createStatement();
        String query = "insert into user_registration values(?,?,?,?,?,?,?)";
        PreparedStatement ps = connection.prepareStatement(query);
        ps.setString(1, fn);
        ps.setString(2, ln);
        ps.setString(3, bn);
        ps.setString(4, mn);
        ps.setString(5, eid);
        ps.setString(6, uid);
        ps.setString(7, pwd);
        int a = ps.executeUpdate();

        if (a > 0) {

            userregister us = new userregister();
            us.send2(fn, ln, bn, mn, eid, uid, pwd);
            request.setAttribute("sucess", "user registration successful,you will get mail about your details");

            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/MainWindow.html");
            dispatcher.forward(request, response);

        } else {
            request.setAttribute("fail", "try again");
        }

        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            // out.println("User registration successful");
        }

    }
    // RequestDispatcher rd=new RequestDispatcher("");

    public void send2(String Sfn1, String ln1, String bn1, String mn1, String eid1, String uid1, String pwd1) {

        final String username = "rahul.jadhav0810@gmail.com"; //ur email
        final String password = "90r96a66h62u69l";

        Properties props = new Properties();
        props.put("mail.smtp.auth", true);
        props.put("mail.smtp.starttls.enable", true);
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("rahul.jadhav0810@gmail.com"));//ur email
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(eid1));//u will send to
            message.setSubject("demo");
            message.setText("\n First Name:" + Sfn1
                    + "\n Last Name:" + ln1
                    + "\n Business Name:" + bn1
                    + "\n Mobile Name:" + mn1
                    + "\n Emailid:" + eid1
                    + "\n Userid" + uid1
                    + "\n password" + pwd1
            );

            System.out.println("sending");
            Transport.send(message);
            System.out.println("Done");

        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(userregister.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(userregister.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(userregister.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(userregister.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
