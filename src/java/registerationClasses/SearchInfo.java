package registerationClasses;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import requriedDatabaseConnection.DatabaseConnection;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author G1 NOTEBOOK
 */
public class SearchInfo extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, SQLException {

        String fname = request.getParameter("fname");
        // out.println("\n fname" + fname);
        String lname = request.getParameter("lname");
        // out.println("\n lname" + lname);
        String aname = request.getParameter("aname");
        // out.println("\n aname" + aname);
        String area = request.getParameter("area");
        String year = request.getParameter("year");

        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();
        Connection connection = DatabaseConnection.getConnection();
         out.println("\n connection established");
        out.println(request.getParameter(aname));
        Statement st = connection.createStatement();
        String query = "select *from client_info";
         out.println("\n query is executed");
        PreparedStatement ps = connection.prepareStatement(query);
          
        ResultSet rs = ps.executeQuery();
      
        while (rs.next()) {
               out.println(rs.getString("first_name"));
                out.println(rs.getString("last_name"));
                    
           
        }
       
    }

    /* TODO output your page here. You may use following sample code. */
    //out.println(request.getParameter("fname"));
    //out.println(request.getParameter("lname"));
    // out.println(request.getParameter("aname"));
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(SearchInfo.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(SearchInfo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(SearchInfo.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(SearchInfo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
