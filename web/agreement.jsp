
<!DOCTYPE html>
<html>
    <head>
        <title></title>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <style type="text/css">

        </style>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <h6 style="color:white;background-color: teal;text-align: center;font-size: 22px;height: 35px;margin-top: 0px;padding: 5px;font-family: calibri;font-weight: bold">Living Concepts Database Management System</h6>

            </div>
            <div class="row">
                <div class="col-lg-12 col-sm-12" style="border: 2px solid teal;width:750px;margin-left: 270px;margin-top: 10px;border-radius: 2px; box-shadow: 5px 10px 18px #888888;margin-bottom: 10px">
                    <form action="agreement.jsp" style="margin-top: 10px;margin-left: 30px;">
                        <div class="col-lg-6">
                            <img src="logo.png" style="width:180px;height: 90px;float: left;margin-left: -50px">

                        </div>
                        <div class="col-lg-6">
                            <h3 style="color:teal;font-weight: bold;font-family: calibri;">Welcome to CRM Database</h3>
                        </div>
                        <div class="col-lg-12">
                            <h3 style="font-weight: bold;font-family: arial;text-align: center;margin-top: -5px">License Agreement</h3><hr>
                            <p style="font-family: calibri;margin-top: -10px">Please read following important information before continuing</p>
                            <p style="font-family: calibri;margin-top: -10px"">Please read the following license Agreement.</p>
                        </div>
                        <div class="col-lg-12" style="border:2px solid teal;overflow: scroll;height: 250px;margin-left: 15px;width:610px">
                            <p style="font-family: calibri;margin-top: 10px;">Please read the following terms and conditions carefully before using this software. <br>
                                Use of this software indicates you accept the terms of this license agreement and<br>
                                warranty.<br><br>

                                <b>1. Disclaimer of Warranty</b><br><br>

                                TeraCopy (this software) is provided "as-is" and without warranty of any kind,<br>
                                express, implied or otherwise, including without limitation, any warranty of<br>
                                merchantability or fitness for a particular purpose. <br><br>

                                In no event shall the author of this software be held liable for data loss, damages,<br>
                                loss of profits or any other kind of loss while using or misusing this software.<br><br><br>

                                <b>2. License</b><br>

                                TeraCopy is provided as FREEWARE for private (non-commercial) or educational <br>
                                (including non-profit organization) use. In these cases, you are granted the right to<br>
                                use and to make an unlimited number of copies of this software.<br><br>

                                For commercial use, it is required to register this software. Registering<br>
                                is an important source of support in the development of future versions.<br><br>

                                <b>3. Restrictions on Use </b><br>

                                This software must not be decompiled, disassembled, reverse engineered or <br>
                                otherwise modified. <br><br>

                                <b>4. Registered Version</b><br>

                                A registered copy of TeraCopy may be used by the licensee exclusively on one <br>
                                or more computers, or used on a network non-simultaneously by multiple people, <br>
                                but not both.<br><br>

                                <b>Copyright (C) 2007-2009 Code Sector Inc. All rights reserved.</b>
                            </p>
                        </div>
                        <div class="col-lg-12">
                            <p style="margin-top: 5px;font-family: calibri">You must accept terms of this agreement,Before Continuing.</p>
                        </div>
                        <div class="col-lg-12" style="font-family: calibri">
                            <label class="radio-inline">
                                <input type="radio" name="accept" value="agreed" id="accept">I accept the agreement</label><br>
                            <label class="radio-inline">
                                <input type="radio" name="accept" value="notagreed" id="reject">I do not accept the agreement</label>
                        </div>

                        <div class="clearfix"></div><hr>

                        <button class="btn" style="color: white;background-color: teal;border-radius: 5px;width: 80px;height: 30px;margin-bottom: 10px;padding: 5px;margin-left: 180px">Next</button>
                        <button class="btn" style="color: white;margin-left:30px;background-color: teal;border-radius: 5px;width: 80px;height: 30px;margin-bottom: 10px;padding: 5px;">Cancel</button>
                    </form>

                    <%
                        String check = request.getParameter("accept");

                        if ("agreed".equals(check)) {
                    %>
                    <jsp:forward page="Userregister.jsp" >  
                        <jsp:param name="name" value="javatpoint.com" />  
                    </jsp:forward>  

                    <% } %>









                </div>
            </div>
        </div>

    </body>
</html>