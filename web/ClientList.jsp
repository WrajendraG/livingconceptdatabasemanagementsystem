
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <style type="text/css">
            * {
                margin:0px;
                padding:0px;
            }
            body {
                font-family:candara;
            }

            #navigation {
                width:100%;
                height:30px;
                background-color:black;
            }
            #navigation ul {
                list-style:none;
            }
            #navigation ul li {
                float:left;
            }
            #navigation ul a {
                display:block;
                height:30px;
                width:120px;
                color:#FFF;
                text-align:center;
                text-decoration:none;
                line-height:25px;
                font-size:15px;
                background-color:black;
            }
            #navigation ul a:hover {
                background-color:teal;
            }
            #navigation ul li ul li {
                float:none;
                background-color: teal;
                color:white;
                font-size: 15px;
                width:140px;
            }
            #navigation ul li ul {
                display:none;
            }

            label{
                color:teal;
                font-size: bold;
            }
            .required{
                color:red;
            }
            .table-bordered{
                border:1px solid black;
            }
            .navbar{
                border-radius: 0px;
            }
        </style>
    </head>
    <body>
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span> 
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
                        <li><a href="index.html">CLIENT INFO</a></li>
                        <li><a href="SearchInfo.jsp">SEARCH INFO</a></li>
                        <li><a href="ClientList.jsp">CLIENT LIST</a></li> 
                        <li><a href="Setting.jsp">SETTINGS</a></li> 
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="main.html"><i class="fa fa-power-off"></i>&nbsp;&nbsp;LOGOUT</a></li>

                    </ul>
                </div>
            </div>
        </nav>
        <h6 style="color:white;background-color: teal;text-align: center;font-size: 20px;height: 30px">Living Concepts Database Management System</h6>
        <div class="container-fluid">
            <div class="row">
                <div class="panel" style="background-color: lightgrey;height:25px">


                    <h6 style="color:teal;text-align: center;font-size: 20px;margin-top:1px;float: left;font-family: calibri;margin-left: 20px">Client List</h6>

                </div>
                <form action="clientinfo" method="POST" id="searchForm">	
                    <div class="row">
                        <table class="table-bordered table-responsive" style="border:1px solid black;border-collapse: collapse;text-align: center;margin-left: 100px;">
                            <tr style="height: 30px;width:40px;font-size:17px;color:teal;font-family: calibri">
                                <td style="width: 80px">Sr.No</td>
                                <td style="width: 200px">Category</td>
                                <td style="width: 200px">Sub Category</td>
                                <td style="width: 270px">Name</td>
                                <td style="width: 250px">Apartment Name</td>
                                <td style="width: 250px">Area</td>
                                <td  style="width: 80px">View</td>
                            </tr>


                            <%

                                Class.forName("oracle.jdbc.driver.OracleDriver");

                                Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "system", "root");

                                Statement st = con.createStatement();

                                String query = "select *from client_info";

                                PreparedStatement ps = con.prepareStatement(query);

                                ResultSet rs = ps.executeQuery();

                                while (rs.next()) {


                            %>
                            <tr style="width: 40px;height: 40px;font-family: calibri;font-size:16px">
                                <td style="width: 80px">srno remain to add</td>
                                <td style="width: 200px">add in client info table</td>
                                <td style="width: 200px"><%=rs.getString("indorcorp")%></td>
                                <td style="width: 270px"><%=((rs.getString("first_name")) + " " + (rs.getString("last_name")))%></td>
                                <td style="width: 250px"><%=rs.getString("apartment_names")%></td>
                                <td style="width: 250px"><%=rs.getString("area")%></td>
                                <td  style="width: 80px"><button class="btn" id="bt1" style="width: 50px;height: 25px;font-family:calibri;font-size: 12px;background-color: teal;color: white;margin-right: 10px;padding: 5px;font-weight: bold;margin-bottom:30px ;margin-top: 30px;margin-left: 10px">View</button></td>
                            </tr>

                            <% }%>
                        </table>
                    </div>
                </form>
                </body>
                </html>