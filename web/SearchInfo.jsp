
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <style type="text/css">
            * {
                margin:0px;
                padding:0px;
            }
            body {
                font-family:candara;
            }

            #navigation {
                width:100%;
                height:30px;
                background-color:black;
            }
            #navigation ul {
                list-style:none;
            }
            #navigation ul li {
                float:left;
            }
            #navigation ul a {
                display:block;
                height:30px;
                width:120px;
                color:#FFF;
                text-align:center;
                text-decoration:none;
                line-height:25px;
                font-size:15px;
                background-color:black;
            }
            #navigation ul a:hover {
                background-color:teal;
            }
            #navigation ul li ul li {
                float:none;
                background-color: teal;
                color:white;
                font-size: 15px;
                width:140px;
            }
            #navigation ul li ul {
                display:none;
            }

            label{
                color:teal;
                font-size: bold;
            }
            .required{
                color:red;
            }
            .table-bordered{
                border:1px solid black;
            }
        </style>
    </head>
    <body>
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span> 
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
                        <li><a href="index.html">CLIENT INFO</a></li>
                        <li><a href="SearchInfo.jsp">SEARCH INFO</a></li>
                        <li><a href="ClientList.jsp">CLIENT LIST</a></li> 
                        <li><a href="Setting.jsp">SETTINGS</a></li> 
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="main.html"><i class="fa fa-power-off"></i>&nbsp;&nbsp;LOGOUT</a></li>

                    </ul>
                </div>
            </div>
        </nav>
        <h6 style="color:white;background-color: teal;text-align: center;font-size: 20px;height: 30px">Living Concepts Database Management System</h6>
        <div class="container-fluid">
            <div class="row">
                <div class="panel" style="background-color: lightgrey;height:25px">


                    <h6 style="color:teal;text-align: center;font-size: 20px;font-weight: bold;margin-top:1px;float: left;font-family: calibri;margin-left: 45px">Client Info</h6>

                </div>
                <form action="SearchInfo.jsp" method="POST" id="searchForm">
                    <div class="container"  style="margin-left:230px">
                        <div class="row">
                            <div class="col-lg-3 col-sm-12" style="">

                                <label for="fname">Select Year</label>
                                <select id="Year" name="year" style="width:255px;height: 31px;border-radius: 5px">
                                    <option value="year">--Select--</option>
                                    <option value="2016">2016</option>
                                    <option value="2015">2015</option>
                                    <option value="2014">2014</option>
                                    <option value="2013">2013</option>
                                    <option value="2012">2012</option>
                                    <option value="2011">2011</option>
                                    <option value="2010">2010</option>
                                    <option value="2009">2009</option>
                                    <option value="2008">2008</option>
                                    <option value="2007">2007</option>
                                    <option value="2006">2006</option>
                                    <option value="2005">2005</option>
                                    <option value="2004">2004</option>

                                </select>

                            </div>
                            <div class="col-sm-12 col-lg-3">

                                <label for="fname">First Name</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                    <input id="fname" type="text" class="form-control" name="fname" placeholder="First Name" style="width:215px;height: 31px;">
                                </div>

                            </div>
                            <div class="col-lg-3 col-sm-12">

                                <label for="lname">Last Name</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                    <input id="lname" type="text" class="form-control" name="lname" placeholder="Last Name" style="width:215px;height: 31px;">
                                </div>

                            </div>
                        </div><br>
                        <div class="row">

                            <div class="col-lg-3 col-sm-12">

                                <label for="Apt">Apartment Names</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                                    <input id="Apt" type="text" class="form-control" name="aprtname" placeholder="Apartments Names" style="width:215px;height: 31px;">
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-12">

                                <label for="Area">Area</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-globe"></i></span>
                                    <select id="Area" type="text" class="form-control" name="area" required="" style="width:215px;height: 31px;">
                                        <option value="volvo">--Select--</option>
                                           <option value="Chakan">Chakan</option>
                                              <option value="Karve nagar">Karve nagar</option>
                                                 <option value="Warje">Warje</option>
                                                    <option value="kothrud">kothrud</option>
                                    </select>
                                </div>

                            </div>
                            <div class="col-lg-3 col-sm-12">
                                <button type="submit" name="search" style="width: 150px;height: 40px;color:white;background-color: teal;border-radius: 7px;text-align: center;font-size: 18px;margin-top: 20px">Search</button>
                            </div><div class="clearfix"></div>
                        </div>
                    </div><br><br>


                    <div class="row">
                        <table class="table-bordered" style="border:1px solid black;border-collapse: collapse;text-align: center;margin-left: 150px;">
                            <tr style="height: 30px;width:40px;font-size:20px;color:teal;font-weight: bold;font-family: calibri">
                                <td style="width: 70px">Sr.No</td>
                                <td style="width: 150px">Category</td>
                                <td style="width: 150px">Sub Category</td>
                                <td style="width: 250px">Name</td>
                                <td style="width: 250px">Apartment Name</td>
                                <td style="width: 250px">Area</td>
                                <td  style="width: 80px"></td>
                            </tr>
                            <%       Class.forName("oracle.jdbc.driver.OracleDriver");

                                Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "system", "root");

                                Statement st = con.createStatement();
                                String query = "select indorcorp,first_name,last_name,area,apartment_names,wings_name from client_info where  year='" + request.getParameter("year") + "' OR first_name='" + request.getParameter("fname") + "' OR last_name='" + request.getParameter("lname") + "' OR apartment_names='" + request.getParameter("aprtname") + "' OR area='" + request.getParameter("area") + "'  ";

                                PreparedStatement ps = con.prepareStatement(query);

                                ResultSet rs = ps.executeQuery();
                                while (rs.next()) {
                            %>

                            <tr style="width: 40px;height: 40px;font-family: arial;font-size:15px">
                                <td>srno remain to add</td>
                                <td>add in client info table</td>
                                <td><%=rs.getString("indorcorp")%></td>


                                <td> <%=((rs.getString("first_name")) + " " + (rs.getString("last_name")))%> </td>

                                <td><%=rs.getString("apartment_names")%></td>
                                <td><%=rs.getString("area")%></td>
                                <td style="width: 80px"><button class="btn" id="bt1" style="width: 50px;height: 25px;font-family:calibri;font-size: 12px;background-color: teal;color: white;margin-bottom:30px;margin-right: 10px;padding: 5px;font-weight: bold;margin-top: 30px;margin-left: 10px">View</button></td>
  





                            </tr>
                            <% }%>
                            </tbody>
                        </table><br>

                    </div>
            </div>
        </form>
</body>
</html>